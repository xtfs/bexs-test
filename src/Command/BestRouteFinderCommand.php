<?php

namespace App\Command;

use App\Core\RouteRepository;
use Exception;
use InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BestRouteFinderCommand extends Command
{
    protected static $defaultName = 'bexs:find';
    protected RouteRepository $routeRepository;

    public function __construct(RouteRepository $routeRepository)
    {
        $this->routeRepository = $routeRepository;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Startup the application.')
            ->setHelp('This command start up the application')
            ->addArgument('fromTo', InputArgument::REQUIRED, 'The from-to route');
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            "This is the best route command",
            "please specify the route that you want to travel",
            "Must be a FROM-TO text",
            "============================"
        ]);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $args = $this->resolveArgument($input->getArgument('fromTo'));
            $bestRoute = $this->routeRepository->findCheaper(...$args);
            $output->writeln([
                'Melhor rota encontrada: ',
                $bestRoute,
                'Obrigado por usar nosso sistema',
                'Enjoy!']);
        } catch (Exception $exception) {
            $output->writeln($exception->getMessage());
        }
        return 0;
    }

    /**
     * Resolve args
     * @param $fromTo
     * @return array
     */
    private function resolveArgument(string $fromTo): array
    {
        $arguments = explode('-', $fromTo);

        if (isset($arguments[1]) === false) {
            throw new InvalidArgumentException('Cannot resolve Arguments, please enter from-to text');
        }

        return [$arguments[0], $arguments[1]];
    }
}