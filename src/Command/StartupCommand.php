<?php 
namespace App\Command;

use App\Service\StartupTravelRoutes;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class StartupCommand extends Command
{
    protected static $defaultName = 'bexs:startup';
    protected StartupTravelRoutes $startupTravelRoutes;

    public function __construct(StartupTravelRoutes $startupTravelRoutes)
    {
        $this->startupTravelRoutes = $startupTravelRoutes;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Startup the application.')
            ->setHelp('This command start up the application')
            ->addArgument('filename', InputArgument::REQUIRED, 'The csv filename');
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            "Welcome to routes finder",
            "This is the startup command, please specify the name(fullpath) of file",
            "============================"
        ]);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $this->startupTravelRoutes->startUp($input->getArgument('filename'));
            $output->writeln('Routes imported!');
        }catch (\Exception $exception) {
            $output->writeln($exception->getMessage());
        }
        return 0;
    }
}