<?php

namespace App\Core;

class RouteRepository extends CustomDb
{
    private string $mainFrom;
    private string $mainTo;

    public array $routes = [];
    private array $csvRoutes = [];
    private array $searchBuffer;

    public function __construct(string $customDb)
    {
        parent::__construct($customDb);
        $this->loadAllRoutes();
    }

    private function loadAllRoutes()
    {
        $handle = fopen($this->customDb . 'routesCsv.csv', 'rb');
        if ($handle === false) {
            throw new \RuntimeException('Cannot open the routes csv file');
        }

        while (($data = fgetcsv($handle, 1000, ',')) !== false) {
            $newRoute = new Route($data[0], $data[1], $data[2]);
            $this->csvRoutes[$newRoute->getId()] = $newRoute;
        }

        fclose($handle);
    }

    public function findCheaper($from, $to): string
    {
        $this->mainFrom = $from;
        $this->mainTo = $to;

        $this->findDirectRoute();

        $this->matchPossibleRoutes($from);
        return json_encode($this->routes);
    }

    /*
     * Fazer outro foreach passando os destinos intermediários, para começar o descarte dos unsets até a origem.
     *
     *  */
    public function matchPossibleRoutes($from)
    {
        foreach ($this->csvRoutes as $id => $route) {
            $csvFrom = $route->getFrom();
            $csvTo = $route->getTo();

            if ($csvFrom === $this->mainFrom && $csvTo === $this->mainTo) {
                continue;
            }

            if ($csvFrom === $from) {
                $this->searchBuffer[] = $route;
                unset($this->csvRoutes[$this->searchBuffer[0]->getId()]);

                if ($csvTo === $this->mainTo) {
                    $this->routes[]['full-route'] = [...$this->searchBuffer];
                    $this->searchBuffer = [];
                }

                $this->matchPossibleRoutes($csvTo);
            }
        }
    }

    private function findDirectRoute(): void
    {
        foreach ($this->csvRoutes as $route) {
            if ($route->getFrom() !== $this->mainFrom || $route->getTo() !== $this->mainTo) {
                continue;
            }

            $this->routes[]['full-route'] = [$route];
            unset($this->csvRoutes[$route->getId()]);
        }
    }
}