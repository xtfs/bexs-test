<?php

namespace App\Core;

final class Route implements \JsonSerializable
{
    private string $id;
    private string $from;
    private string $to;
    private float $price;

    public function __construct(string $from, string $to, float $price)
    {
        $this->id = uniqid($from . $to, true);
        $this->from = $from;
        $this->to = $to;
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFrom(): string
    {
        return $this->from;
    }

    /**
     * @return string
     */
    public function getTo(): string
    {
        return $this->to;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'from' => $this->getFrom(),
            'to' => $this->getTo(),
            'price' => (string)$this->getPrice(),
        ];
    }
}