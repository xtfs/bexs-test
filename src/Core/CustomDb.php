<?php

namespace App\Core;

abstract class CustomDb
{
    protected string $customDb;

    public function __construct(string $customDb)
    {
        $this->customDb = $customDb;
    }
}