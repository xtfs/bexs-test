<?php

namespace App\Service;

use RuntimeException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class StartupTravelRoutes
{
    private string $customDb;

    /**
     * StartupTravelRoutes constructor.
     * @param string $customDb
     */
    public function __construct(string $customDb)
    {
        $this->customDb = $customDb;
    }

    /**
     * @param string $filename
     * @return bool
     */
    public function startUp(string $filename): bool
    {
        $this->validateFile($filename);

        if (!is_dir($this->customDb) && !mkdir($this->customDb, 0777, true)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $this->customDb));
        }

        if (file_exists($this->customDb. 'routesCsv.csv')) {
            unlink($this->customDb . 'routesCsv.csv');
        }

        return copy($filename, $this->customDb . 'routesCsv.csv');
    }

    /**
     * Check and validate whether the file exists
     * @param $filename
     * @throws RuntimeException
     */
    private function validateFile($filename): void
    {
        if (false === file_exists($filename)) {
            throw new RuntimeException("The filename '{$filename}' doesn't exists");
        }
    }
}